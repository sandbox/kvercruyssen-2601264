-- SUMMARY --

The YMLP module creates a subscription block for your YMLP newsletter.
If you have multiple groups in one YMLP account, a dropdown will be provided to the user.

-- REQUIREMENTS --

* YMLP account
  A YMLP account which can be created on http://www.ymlp.com?a=HP88CC
  This link provides us with a referral fee which will support the development of this module.

* API access
  Activate API access at http://www.ymlp.com/app/api.php

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Administer YMLP
    The user will be able to administer the YMLP settings

* Edit your settings on Administration » Configuration » YMLP configuration

  - Enter your API key and username

  - Test the connection by clicking "YMLP Connection Test"
    Your credentials are tested and active groups are loaded.

* Active the "YMLP Subscription block" on Administration » Structure » Blocks




-- CONTACT --

Current maintainer:
* Kristof Vercruyssen (kvercruyssen) - http://drupal.org/user/378510

This project has been sponsored by:
* Trivali webcommunicatie
  A Belgian based agency which offersdevelopment, theming and customization services.
  Visit us at http://www.trivali.be