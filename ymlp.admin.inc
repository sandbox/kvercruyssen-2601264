<?php

/**
 * Implements hook_admin_settings()
 */
function ymlp_admin_settings() {
  $form['ymlp'] = array(
    '#type' => 'fieldset',
    '#title' => 'YMLP',
  );

  $form['ymlp']['ymlp_debug_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => 'Debug enabled',
    '#description' => t('If enabled, all actions are written to watchdog'),
    '#default_value' => variable_get('ymlp_debug_enabled', 0),
  );

  $form['ymlp']['ymlp_apikey'] = array(
    '#type' => 'textfield',
    '#title' => 'API key',
    '#default_value' => variable_get('ymlp_apikey', ''),
    '#description' => 'Activate API access at http://www.ymlp.com/app/api.php and paste your API key here.',
  );

  $form['ymlp']['ymlp_username'] = array(
    '#type' => 'textfield',
    '#title' => 'Username',
    '#default_value' => variable_get('ymlp_username', ''),
  );

  if (count(variable_get('ymlp_groups', array()))) {
    $form['ymlp']['ymlp_active_groups'] = array(
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#title' => 'Active groups',
      '#description' => 'Groups on which the user can subscribe. If no groups are visible, test the connection.',
      '#default_value' => variable_get('ymlp_active_groups', array()),
      '#options' => variable_get('ymlp_groups', array()),
    );

    if (count(variable_get('ymlp_active_groups', array()))) {
      $form['ymlp']['ymlp_default_groups'] = array(
        '#type' => 'checkboxes',
        '#required' => TRUE,
        '#title' => 'Default groups',
        '#description' => 'Default groups to which the user is subscribed. If multiple groups are selected, the mail address will be subscribed to all of them.',
        '#default_value' => variable_get('ymlp_default_groups', array()),
        '#options' => variable_get('ymlp_groups', array()),
      );
    } else {
      $form['ymlp']['no_active_groups'] = array(
        '#type' => 'item',
        '#title' => 'Default groups',
        '#description' => 'Select at least one active group and save the page.',
      );
    }
  } else {
    $form['ymlp']['no_groups'] = array(
      '#type' => 'item',
      '#title' => 'Active groups',
      '#description' => 'Please save the form and load the groups by testing the connection.',
    );
  }

  $form['ymlp_form'] = array(
    '#type' => 'fieldset',
    '#title' => 'YMLP form settings',
    '#description' => t('Configuration for the subscription form, in English!'),
  );

  $form['ymlp_form']['ymlp_form_title'] = array(
    '#type' => 'textfield',
    '#title' => 'Title',
    '#default_value' => variable_get('ymlp_form_title', 'Subscribe to our newsletter'),
  );

  $form['ymlp_form']['ymlp_form_placeholder'] = array(
    '#type' => 'textfield',
    '#title' => 'Placeholder',
    '#default_value' => variable_get('ymlp_form_placeholder', 'Your email address'),
  );

  $form['ymlp_form']['ymlp_form_submit'] = array(
    '#type' => 'textfield',
    '#title' => 'Submit button',
    '#default_value' => variable_get('ymlp_form_submit', 'Subscribe'),
  );

  return system_settings_form($form);
}

/**
 * ymlp_admin_test_connection()
 *
 * Tests the connection with YMLP
 * Loads the groups
 */
function ymlp_admin_test_connection() {
  $ymlp_groups = array();
  $ymlp = new ymlp(variable_get('ymlp_apikey', ''), variable_get('ymlp_username', ''));

  if ($ymlp->ping()) {

    // While we're here, get the groups
    if (count($groups = $ymlp->GroupsGetList())) {
      foreach($groups AS $group) {
        $ymlp_groups[$group['ID']] = $group['GroupName'];
      }

      variable_set('ymlp_groups', $ymlp_groups);
      drupal_set_message(t('!count groups synced', array('!count' => count($ymlp_groups))), 'status');
    }

    drupal_set_message(t('YMLP Connection successful!'), 'status');
  } else {
    drupal_set_message(t('YMLP Connection failed! Error: !error', array('!error' => $ymlp->ErrorMessage)));
  }

  drupal_goto('admin/config/ymlp/settings');
}